package zh

import org.scalatest.concurrent.{Signaler, TimeLimitedTests}
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.time.{Millis, Span}

import scala.util.Random

class CountTest extends AnyFlatSpec with TimeLimitedTests {
  val timeLimit: Span = Span(1000, Millis)
  override val defaultTestSignaler: Signaler = new Signaler {
    override def apply(testThread: Thread): Unit = {
      println("Ez a teszt túl sokáig fut.")
      testThread.stop()
    } //unsafe, never használd.
  }
  
  "CountTest" should "for a JSONString" in {
    val input = JSONString("tibi")
    val result = input count { _.isInstanceOf[JSONString] }
    val expected = 1
    assert(result == expected, s"$input count { _.isInstanceOf[JSONString] } eredménye $result lett, $expected kéne legyen!")    
    val result2 = input count { _.isInstanceOf[JSONArray] }
    val expected2 = 0
    assert(result2 == expected2, s"$input count { _.isInstanceOf[JSONArray] } eredménye $result lett, $expected kéne legyen!")    
  }
  
  it should "for an array of JSONStrings" in {
    val input = JSONArray(Vector(JSONString("feri"), JSONString("robi"), JSONString("zoli")))
    val result = input count { _.isInstanceOf[JSONString] }
    val expected = 3
    assert(result == expected, s"$input count { _.isInstanceOf[JSONString] } eredménye $result lett, $expected kéne legyen!")
  } 
  
  it should "for a complex JSON Object" in { 
    val input = JSONObject(Vector(("name",JSONString("feri")), ("age", JSONString("42")), ("data", JSONObject(Vector(("field",JSONString("none")),("lottery",JSONArray(Vector(JSONString("13"),JSONString("42")))))))))
    val result = input count { _.isInstanceOf[JSONString] }
    val expected = 5
    assert(result == expected, s"$input count { _.isInstanceOf[JSONString] } eredménye $result lett, $expected kéne legyen!")
  }
  
}

